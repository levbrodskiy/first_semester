package general.binaryexpression;

import general.exceptions.IntOverflowException;
import general.exceptions.NullExсeption;

public class BinaryExpression {
    private int a;
    private int b;
    private char operation;

    public BinaryExpression(int a, int b, char operation) {
        this.a = a;
        this.b = b;
        this.operation = operation;
    }

    public char getOperation() {
        return operation;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    private int add(int a, int b) throws IntOverflowException {
        int r = a + b;
        if (((a ^ r) & (b ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию разностьи первого и второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return разность a и b
     * @throws ArithmeticException переполнение
     */
    private int sub(int a, int b) throws IntOverflowException {
        int r = a - b;
        if (((a ^ b) & (a ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию деления первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return результат деления a на b
     * @throws ArithmeticException деление на ноль
     */
    private int div(int a, int b) throws NullExсeption {
        if (b == 0) {
            throw new NullExсeption();
        }

        return a / b;
    }

    /**
     * Реализует операцию произведения первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return произведение a и b
     * @throws ArithmeticException переполнение
     */
    private int multiply(int a, int b) throws IntOverflowException {
        long r = (long) a * (long) b;
        if ((int) r != r) {
            throw new IntOverflowException();
        }

        return (int) r;
    }

    private int divOst(int a, int b) throws NullExсeption {
        if (b == 0) {
            throw new NullExсeption();
        }

        return a % b;
    }

    /**
     * Реализует операцию возведения первого операнда в степень второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return a в степени b
     * @throws ArithmeticException переполнение
     */
    private int pow(int a, int b) throws IntOverflowException {
        long c = 1;
        for (int i = 0; i < b; i++) {
            c = c * a;
        }
        if ((int) c != c) {
            throw new IntOverflowException();
        }
        return (int) c;
    }

    /**
     * Метод определяет операцию по значению знака и возвращает результат операции
     *
     * @return результат операции
     */
    public int calculate() throws NullExсeption, IntOverflowException {

        int result = 0;
        try {

            switch (operation) {

                case '*': {
                    result = multiply(a, b);
                    break;
                }

                case '/': {
                    result = div(a, b);
                }
                break;

                case '-': {
                    result = sub(a, b);
                    break;
                }

                case '+': {
                    result = add(a, b);
                    break;
                }

                case '^': {
                    result = pow(a, b);
                    if (b == 0) {
                        result = 1;
                    }
                    break;
                }

                case '%': {
                    result = divOst(a, b);
                    break;
                }
            }
        }catch (NullExсeption e){
            throw new NullExсeption();
        }
        catch (IntOverflowException e) {
            throw new IntOverflowException();
        }
        return result;
    }
}
