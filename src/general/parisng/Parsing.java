package general.parisng;

import general.binaryexpression.BinaryExpression;
import general.exceptions.ParsingException;

public class Parsing {
    public static BinaryExpression parsingToBinary(String s) throws ParsingException {
        String[] arr = s.split("\\s");
        int a;
        int b;
        char operation;
        try {
             a = Integer.parseInt(arr[0]);
             b = Integer.parseInt(arr[2]);
             operation = arr[1].charAt(0);
        }catch (Exception e){
            throw new ParsingException();
        }

        return new BinaryExpression(a, b, operation);
    }
}
