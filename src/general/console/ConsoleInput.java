package general.console;

import java.util.Scanner;

public class ConsoleInput {

    public static String scan(){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        sc.close();
        return s;
    }
}
