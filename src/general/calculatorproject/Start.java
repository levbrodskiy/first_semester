package general.calculatorproject;

import general.binaryexpression.BinaryExpression;
import general.console.ConsoleInput;
import general.exceptions.IntOverflowException;
import general.exceptions.NullExсeption;
import general.exceptions.ParsingException;
import general.mathinteger.MathInteger;
import general.parisng.Parsing;
import general.regularexpressions.RegularСonstants;
import general.validation.Validation;

public class Start {

    public static void main(String[] args) {
        String s = ConsoleInput.scan();
        try {
            Validation.validationBinaryOperation(s, RegularСonstants.REG_BINARY_OPERATION);

        }catch (NumberFormatException e){
            System.out.println("Введены неверные данные!");
            return;
        }
            BinaryExpression operation;
        try {
            operation = Parsing.parsingToBinary(s);
        }catch (ParsingException e){
            System.out.println("Ошибка парсинга!");
            return;
        }
        try {
            System.out.println(MathInteger.doOperation(operation));
        }catch (NullExсeption e){
            System.out.println("Ошибка деления на 0!");
            return;
        }
        catch (IntOverflowException e){
            System.out.println("Ошибка переполнения!");
            return;
        }
        catch (Exception e){
            System.out.println("Неопознанная ошибка!");
            return;
        }
    }

}
