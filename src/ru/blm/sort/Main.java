package ru.blm.sort;

import java.util.Arrays;

/**
 * @author Лев Бродский 17ИТ18
 * Основной класс Main
 */
public class Main {

    public static void main(String[] args) {
        int[] arr = {0, 2, 1, 1, 0, 2, 2, 1};
        System.out.println("Отсортированный массив: " + Arrays.toString(Sort.sort(arr)));
    }

}
