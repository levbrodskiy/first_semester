package ru.blm.bank;

/**
 * Класс наследник интерфейса, реализующий работу с потоком снятия с баланса
 */

public class WithdrawThread implements Runnable {

    private Account account;

    /**
     * Конструктор класса
     * @param account - ссылка на объект
     */
    WithdrawThread(Account account) {
        this.account = account;
    }
    /**
     * Метод испольнитель потока
     */
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                account.withdraw(getRandomNumber(1000, 1));
            }catch (Exception e) {

            }
        }

    }

    /**
     * Метод получения случайного числа
     * @param range - первый предел
     * @param min - второй предел
     * @return
     */
    private long getRandomNumber(long range, long min) {
        long a = min + (long) (Math.random() * range);
        return a;
    }
}