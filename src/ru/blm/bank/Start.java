package ru.blm.bank;

/**
 * Программа разработнна для представления банковского аккаунта
 */
public class Start {
    public static void main(String[] args)  {

        Account account = new Account();
        Thread threadDeposit = new Thread(new DepositThread(account));
        Thread threadWithdraw = new Thread(new WithdrawThread(account));
        threadDeposit.start();
        threadWithdraw.start();


    }
}
