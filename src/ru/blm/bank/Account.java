package ru.blm.bank;

/**
 * Класс Account содержит поле balance для размещения баланса банковского счета, а также методы для пополнения баланса и снятия денег со счета.
 */
public class Account {

    private long balance;

    /**
     * Создаёт объект
     */
    public Account() {
        this(0);
    }

    /**
     * Конструктор класса с параметорами
     * @param balance - первоначальный баланс
     */
    public Account(long balance) {
        this.balance = balance;
    }

    /**
     * пополняет баланс
     *
     * @param money - сумма пополнения баланса
     * @throws NegativeException
     */
    public synchronized void deposit(long money) throws NegativeException {
        if (!isPositive(money)) {
            throw new NegativeException();
        }
        balance += money;

    }

    /**
     * снимает с боланса
     *
     * @param money - сумма снятия с баланса
     * @throws OutOfBalanceException
     * @throws NegativeException
     */
    public synchronized void withdraw(long money) throws OutOfBalanceException, NegativeException {
        if (!isPositive(money)) {
            throw new NegativeException();
        }
        if (money > balance) {
            throw new OutOfBalanceException();
        }
        balance -= money;
    }

    /**
     * возвращает значение баланса
     * @return - значение баланса
     */
    public synchronized long getBalance() {
        return balance;
    }

    private boolean isPositive(long numb) {
        return numb >= 0;
    }

}