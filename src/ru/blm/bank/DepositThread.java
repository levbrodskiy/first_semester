package ru.blm.bank;

/**
 * Класс наследник интерфейса, реализующий работу с потоком пополнения баланса
 */

public class DepositThread implements Runnable {

    private Account account;
    /**
     * Конструктор класса
     * @param account - ссылка на объект
     */
    DepositThread(Account account) {
        this.account = account;
    }
    /**
     * Метод испольнитель потока
     */
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                account.deposit(getRandomNumber(100, 0));
            } catch (NegativeException e) {

            }
        }
    }
    /**
     * Метод получения случайного числа
     * @param range - верхний предел
     * @param min - нижний предел
     * @return
     */
    private long getRandomNumber(long range, long min) {
        long a = min + (long) (Math.random() * range);
        return a;
    }
}