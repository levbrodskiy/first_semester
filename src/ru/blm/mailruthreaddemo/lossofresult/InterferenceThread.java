package ru.blm.mailruthreaddemo.lossofresult;

/**
 * Класс для инкрементирования общей переменной
 */

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static int i;
    private static Object lock;
    InterferenceThread(InterferenceExample checker, Object lock) {
        this.checker = checker;
        this.lock = lock;
    }

    private static void increment() {
        synchronized (lock){
            i++;
        }
    }

    int getI() {
        return i;
    }

    public void run() {
        while (!checker.stop()) {
            increment();
        }
    }
}