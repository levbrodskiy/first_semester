package ru.blm.mailruthreaddemo.nonlossofresult;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Пример совместного использования ресурсов
 */
class InterferenceExample {
    private static final int HUNDRED_MILLION = 100_000_000;
    private AtomicInteger counter = new AtomicInteger();
    private StateObject object = new StateObject();
    boolean stop(){
        return counter.incrementAndGet() > HUNDRED_MILLION;
    }

    void example() throws InterruptedException {
        InterferenceThread thread1 = new InterferenceThread(this, object);
        InterferenceThread thread2 = new InterferenceThread(this, object);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Expected: " + HUNDRED_MILLION);
        System.out.println("Result: " + StateObject.getI());


    }
}