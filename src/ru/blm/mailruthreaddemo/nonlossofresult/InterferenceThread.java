package ru.blm.mailruthreaddemo.nonlossofresult;

/**
 * Класс для инкрементирования общей переменной
 */

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static int i;
    private final StateObject object;
    InterferenceThread(InterferenceExample checker, StateObject object) {
        this.checker = checker;
        this.object = object;
    }

    public void run() {
        while (!checker.stop()) {
            object.increment();
        }
    }
}