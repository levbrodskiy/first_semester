package ru.blm.mailruthreaddemo.nonlossofresult;

public class StateObject {
    private static int i;

    synchronized void increment(){
        i++;
    }

    public static int getI(){
        return i;
    }
}
