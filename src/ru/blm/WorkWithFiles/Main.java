package ru.blm.WorkWithFiles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Бродский Лев 17ИТ18
 * Программа для работы с классом Mathinteger
 * Класс реализующий ввод данных через консоль/файл
 *
 */

public class Main {
    /**
     * Основной метод Main для взаимодействия с пользоваателем/файлом
     * @param args
     */
    public static void main(String args[]) {
        ArrayList<String> listWithEqualizations;

        String[] equalizationF;
        String changeType;
        int resultOperation;
        String operation;
        ArrayList<String> arrayListFile = new ArrayList<>();

        System.out.println("Выберите способ ввода console - через консоль; file - через файлы");
        Scanner sc = new Scanner(System.in);
        String dataEnter = sc.next();
        int arr[];

        switch (dataEnter) {

            case "console":
                System.out.println("Введите пример вида (5 + 1)(5 - 1)(5 / 1)(5 * 1)(5 ^ 1)(5 % 1) : ");
                String[] equalization = new String[3];
                try {
                    equalization = scan();
                } catch (NumberFormatException e) {

                }
                if (equalization[0] == null) {

                    System.out.println("Введено неверно!");
                } else {
                    try {
                        arr = parse(equalization);
                        System.out.println(MathIntegerF.doOperation(arr, equalization[1]));
                    } catch (Exception e) {
                        System.out.println("Введено неверно!");
                    }
                }
                break;

            case "file":
                try {
                    listWithEqualizations = WorkWithFile.readFile("file.txt");

                } catch (IOException e) {
                    System.out.println("НЕ УДАЛОСЬ ПРОЧИТАТЬ ФАЙЛ(file not found)");
                    break;
                } catch (Exception e) {
                    break;
                }


                for (String i : listWithEqualizations) {
                    if(verification(i)){
                        equalizationF = i.split(" ");
                        operation = equalizationF[1];
                        try {
                            arr = parse(equalizationF);
                            resultOperation = MathIntegerF.doOperation(arr, operation);
                            changeType = String.valueOf(resultOperation);
                            arrayListFile.add(changeType);
                        } catch (Exception e) {
                            arrayListFile.add("fail");
                        }

                    }
                    else {
                        arrayListFile.add("fail");
                    }

                }

                try {
                    WorkWithFile.writeFile(arrayListFile,"output.txt");
                } catch (IOException e) {
                    System.out.println("НЕ УДАЛОСЬ ВЫВЕСТИ В ФАЙЛ( file not found )");
                    break;
                }
                break;

            default:
                System.out.println("Некорректный ввод");
                break;
        }


    }

    /**
     * Метод parse реализующий перевод стринговых значений в интовые
     * @param equalization - массив с выражением
     * @return - массив с интовыми значениями
     * @throws Exception
     */
    public static int[] parse(String equalization[]) throws Exception {
        int arr[] = new int[2];
        try {
            arr[0] = Integer.parseInt(equalization[0]);
            arr[1] = Integer.parseInt(equalization[2]);
        } catch (Exception e) {
            throw new Exception();
        }
        return arr;
    }

    /**
     * Метод scan для сканировния строки с консоли и разбиения по элементам
     * @return стринговый массив с выражением разбитым по пробелам
     * @throws NumberFormatException
     */
    public static String[] scan() throws NumberFormatException {
        Scanner sc = new Scanner(System.in);
        String scString = sc.nextLine();
        if (verification(scString)) {
            String[] scStrings = scString.split(" ");
            return scStrings;
        } else {
            throw new NumberFormatException();
        }

    }

    /**
     * Метод verification релизующий проверку введенного выражения на корректность
     * @param string - строка выражения
     * @return - результат проверки
     */
    public static boolean verification(String string) {
        if (string.matches("[-+]?[0-9]+\\s[/*-+^%]{1}\\s[-+]?[0-9]+")) {
            return true;
        } else return false;
    }

}
