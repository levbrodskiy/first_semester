package ru.blm.WorkWithFiles;

/**
 * Класс реализует математические операции
 */
public class MathIntegerF {
    /**
     * Реализует операцию сложения первого и второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return сумму a и b
     * @throws ArithmeticException переполнение
     */
    public static int add(int a, int b) throws IntOverflowException {
        int r = a + b;
        if (((a ^ r) & (b ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию разностьи первого и второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return разность a и b
     * @throws ArithmeticException переполнение
     */
    public static int sub(int a, int b) throws IntOverflowException {
        int r = a - b;
        if (((a ^ b) & (a ^ r)) < 0) {
            throw new IntOverflowException();
        }
        return r;
    }

    /**
     * Реализует операцию деления первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return результат деления a на b
     * @throws ArithmeticException деление на ноль
     */
    public static int div(int a, int b) throws NullExeption {
        if (b == 0) {
            throw new NullExeption();
        }

        return a / b;
    }

    /**
     * Реализует операцию произведения первого операнда на второй
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return произведение a и b
     * @throws ArithmeticException переполнение
     */
    public static int multiply(int a, int b) throws IntOverflowException {
        long r = (long) a * (long) b;
        if ((int) r != r) {
            throw new IntOverflowException();
        }

        return (int) r;
    }

    public static int divOst(int a, int b) throws NullExeption {
        if (b == 0) {
            throw new NullExeption();
        }

        return a % b;
    }

    /**
     * Реализует операцию возведения первого операнда в степень второго операнда
     *
     * @param a первый операнд
     * @param b второй операнд
     * @return a в степени b
     * @throws ArithmeticException переполнение
     */
    public static int pow(int a, int b) throws IntOverflowException {
        long c = 1;
        for (int i = 0; i < b; i++) {
            c = c * a;
        }
        if ((int) c != c) {
            throw new IntOverflowException();
        }
        return (int) c;
    }

    /**
     * Метод определяет операцию по значению знака и возвращает результат операции
     *
     * @param arr       массив с данными
     * @param operation знак операции
     * @return результат операции
     */
    public static int doOperation(int arr[], String operation) throws Exception {
        int a = arr[0];
        int b = arr[1];

        int result = 0;
        switch (operation) {
            case "*": {
                try {
                    result = MathIntegerF.multiply(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();

                }
                break;
            }
            case "/": {
                try {
                    result = MathIntegerF.div(a, b);
                } catch (NullExeption e) {
                    throw new NullExeption();

                }
                break;
            }
            case "-": {
                try {
                    result = MathIntegerF.sub(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();

                }
                break;
            }
            case "+": {
                try {
                    result = MathIntegerF.add(a, b);
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();

                }
                break;
            }
            case "^": {
                try {
                    result = MathIntegerF.pow(a, b);
                    if (b == 0) {
                        result = 1;
                    }
                } catch (IntOverflowException e) {
                    throw new IntOverflowException();

                }
                break;
            }
            case "%": {
                try {
                    result = MathIntegerF.divOst(a, b);
                } catch (NullExeption e) {
                    throw new NullExeption();
                }
                break;
            }
        }

        return result;
    }
}
