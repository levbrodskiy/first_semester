package ru.blm.WorkWithFiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Бродский Лев 17ИТ18
 * Класс WorkEithFile реализующий работу с файлом
 */
public class WorkWithFile {
    /**
     * Метод readFile реализующий чтение данных из 1 файла
     * @param way - путь до файла
     * @return выржение
     * @throws IOException
     */
    public static ArrayList<String> readFile(String way) throws IOException {
        ArrayList<String> listWithEqualizations = new ArrayList<>();
        try (BufferedReader buffer = new BufferedReader((new FileReader(way)))) {
            String string;

            while ((string = buffer.readLine()) != null) {
                listWithEqualizations.add(string);
            }
        } catch (IOException e) {
            throw new IOException();

        }
        if (listWithEqualizations != null) {
            return listWithEqualizations;
        } else {
            throw new IOException();
        }
    }

    /**
     * Метод writeFile записывет результат в файл
     * @param arrayList - результаты выражений
     * @param way* - путь к файлу
     * @throws IOException
     */
    public static void writeFile(ArrayList<String> arrayList,String way) throws IOException {
        FileWriter fw = new FileWriter(way);
        try{
            for (String i:arrayList) {
                fw.write(i+"\n");
            }

        }
        catch (Exception e){
            throw new IOException();
        }
        finally {
            fw.close();

        }
    }
}
