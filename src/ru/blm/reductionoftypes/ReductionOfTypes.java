package ru.blm.reductionoftypes;

public class ReductionOfTypes {
    static int g;
    static boolean h;
    static char ch;
    public static void main(String[] args) {
        byte byte_ = 120; // от -128 до 127
        short short_ = 8_000; // от -32768 до 32767
        int int_ = 3_200_000; // от -2147483648 до 2147483647
        long long_ = 999_999_999; // от –9 223 372 036 854 775 808 до 9 223 372 036 854 775 807
        float float_ = 222_222.5f; // -3.4*1038 до 3.4*1038
        double double_ = 999_999.555555555; // ±4.9*10-324 до ±1.8*10308
        char char_ = 'a'; // от 0 до 65536
        boolean boolean_ = true; // true или false
        byte a = (byte) short_; // 64
        int b = short_;
        double c = 128;
        byte d = (byte) a; // -128
        char e = (char) int_; // 퐀
        int f = e; // 54272
        int g = getG(); // 0
        boolean isH = getH(); // false
        char cha = getCh();
        System.out.println(cha); // пробельный символ
    }
   static int getG(){
        return g;
    }

    static boolean getH(){
        return h;
    }

    static char getCh(){
        return ch;
    }
}
