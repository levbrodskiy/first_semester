package ru.blm.copyfilewithnio;

import static ru.blm.copyfile.Constants.*;

public class Start {
    public static void main(String[] args) {


        long sequenceCopyTimeBeforeWithNIO = System.currentTimeMillis();

        NewThreadWithNIO copyNIOSequence1 = new NewThreadWithNIO(inputWay1, outputWay1);
        copyNIOSequence1.run();
        NewThreadWithNIO copyNIOSequence2 = new NewThreadWithNIO(inputWay2, outputWay2);
        copyNIOSequence2.run();

        long sequenceCopyTimeAfterWithNIO = System.currentTimeMillis();

        consoleTime(sequenceCopyTimeBeforeWithNIO, sequenceCopyTimeAfterWithNIO);

        long parallelCopyTimeBeforeWithNIO = System.currentTimeMillis();

        NewThreadWithNIO copyNIOThread1 = new NewThreadWithNIO(inputWay1, outputWay1);
        NewThreadWithNIO copyNIOThread2 = new NewThreadWithNIO(inputWay1, outputWay1);

        copyNIOThread1.thread.start();
        copyNIOThread2.thread.start();

        try {
            copyNIOThread1.thread.join();
            copyNIOThread2.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long parallelCopyTimeAfterWithNIO = System.currentTimeMillis();
        consoleTime(parallelCopyTimeBeforeWithNIO, parallelCopyTimeAfterWithNIO);
    }

    public static void consoleTime(long before, long after){
        System.out.println("copy "+(after - before)+" ms");

    }
}
