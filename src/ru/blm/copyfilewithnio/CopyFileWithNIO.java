package ru.blm.copyfilewithnio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class CopyFileWithNIO {


    private  Path way1;
    private Path way2;

    CopyFileWithNIO(String way1, String way2){
        this.way1 = Paths.get(way1);
        this.way2 = Paths.get(way2);
    }

    public void copy(){
        try {
            Files.copy(way1, way2, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e) {

        }
    }
}
