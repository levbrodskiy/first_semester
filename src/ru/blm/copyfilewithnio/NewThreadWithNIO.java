package ru.blm.copyfilewithnio;

public class NewThreadWithNIO implements Runnable {
    private String way1;
    private String way2;
    Thread thread;
    NewThreadWithNIO(String way1,String way2){
        this.way1 = way1;
        this.way2 = way2;
        thread = new Thread(this,"thread");
    }
    @Override
    public void run() {

        long a = System.currentTimeMillis();
        CopyFileWithNIO cf = new CopyFileWithNIO(way1, way2);

        cf.copy();

        long b = System.currentTimeMillis();

        System.out.println(b - a);
    }
}