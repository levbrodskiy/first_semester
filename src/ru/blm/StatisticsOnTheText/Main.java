package ru.blm.StatisticsOnTheText;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        StringBuffer sb;
        try (BufferedReader buffer = new BufferedReader((new FileReader("file.txt")))) {
            sb = new StringBuffer();

            String string;
            while ((string = buffer.readLine()) != null) {
                sb.append(string);
            }
        } catch (IOException e) {
            throw new IOException();

        }
        System.out.println(sb);
        String s = new String(sb);
        char array[] = s.toCharArray();
        System.out.println(array.length);


        }
    }

