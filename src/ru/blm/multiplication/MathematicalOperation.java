package ru.blm.multiplication;

/**
 * Класс реализует операцию умножения без умножения
 */
public class MathematicalOperation{
    /**
     * Функция вычисления произведения двух целых цисел
     *
     * @param firstOperand - первый элемент произведения
     * @param secondOperand - второй элемент произведения
     * @return возвращает результат произвдения и firstOperand secondOperand
     */
    public static int multiplication(int firstOperand,int secondOperand){

        int loopsCount = 0;
        int numbPlus = 0;
        boolean numbIsInvert = false;
        if(((firstOperand<0)&&(secondOperand>=0))||((secondOperand<0)&&(firstOperand>=0))){
            numbIsInvert = true;
        }
        if(firstOperand > secondOperand){
            loopsCount = Math.abs(secondOperand);
            numbPlus = Math.abs(firstOperand);
        }else {
            loopsCount = Math.abs(firstOperand);
            numbPlus = Math.abs(secondOperand);
        }
        int resultMultiplication = 0;
        for(int i = 0;i < loopsCount;i++){
            resultMultiplication += numbPlus;
        }
        if(numbIsInvert){
            resultMultiplication=-resultMultiplication;
        }
        return resultMultiplication;

    }
}
