package ru.blm.threads;

/**
 *
 */
public class GreatDispute implements Runnable{

    String name;
    /**
     *
     * @param name имя потока
     */
    public GreatDispute(String name){
        Thread.currentThread().setName(name);
        this.name = name;
    }

    /**
     *
     */
    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);

        }
    }
}
