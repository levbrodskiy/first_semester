package ru.blm.threads;

public class Start {
    public static void main(String[] args) {
        Thread egg = new Thread(new GreatDispute("egg"));
        Thread chicken = new Thread(new GreatDispute("chicken"));
        egg.start();
        chicken.start();

        try {
            egg.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (chicken.isAlive()){
            System.out.println("Chicken appeared the first!");
        }else{
            System.out.println("Egg appeared the first!");
        }
    }
}
