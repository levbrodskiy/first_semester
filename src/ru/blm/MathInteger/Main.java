package ru.blm.MathInteger;

import java.util.Scanner;

/**
 * класс реализующий работу над вводом
 */
public class Main {

    public static void main(String args[]) throws Exception {

        System.out.println("Введите пример вида: 5 + 1 Допустимые операции( * / + - % ^)");
        String[] scStrings = new String[3];
        try {
            scStrings = scan();
        } catch (NumberFormatException e) {

        }
        if (scStrings[0] == null) {
            System.out.println("Введено неверно!");
        } else {
            int arr[] = parse(scStrings);
            System.out.println(MathInteger.doOperation(arr, scStrings[1]));
        }


    }

    /**
     *
     * Метод для выделения из массива чисел
     * @param scStrings стринговый массив изначльной строки
     * @return массив с двумя элементами
     */
    public static int[] parse(String scStrings[]) {
        int arr[] = new int[2];
        arr[0] = Integer.parseInt(scStrings[0]);
        arr[1] = Integer.parseInt(scStrings[2]);
        return arr;
    }

    /**
     * метод реализующий ввод выражения с консоли
     * @return возврщает массив строк выражения
     * @throws NumberFormatException
     */
    public static String[] scan() throws NumberFormatException {
        Scanner sc = new Scanner(System.in);
        String scString = sc.nextLine();
        System.out.print("");

        if (scString.matches("[-+]?[0-9]+\\s[/*-+^%]{1}\\s[-+]?[0-9]+")) {
            String[] scStrings = scString.split(" ");
            return scStrings;
        } else {
            throw new NumberFormatException();
        }

    }

}