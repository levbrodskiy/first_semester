package ru.blm.chocolate;

/**
 * Класс реализует подсчёт шоколадок
 */
class Chocolate{
    /**
     * Функция вычисления количества шоколадок
     *
     * @param money - количество денег
     * @param price - цена одной шоколадки
     * @param wrap - количество обёрток, для получения одной шоколадки
     * @return возвращает количество полученных шоколадок или ошибку отрицательного результата(-111)
     */
    public static int chocolateCount(int money, int price, int wrap){
        if((money<0)||(price<0)||(wrap<0)){
            System.out.println("Введены отрицательные значения!");
            return -111;
        }else {return (money/price) + (money/price)/wrap ;}
    }

}