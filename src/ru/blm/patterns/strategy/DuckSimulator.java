package ru.blm.patterns.strategy;

import ru.blm.patterns.strategy.classes.*;

public class DuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.display();
        mallard.perfomFly();
        mallard.perfomQuack();
        Duck model = new ModelDuck();
        model.display();
        model.perfomQuack();
        model.perfomFly();
        model.setFlyBehavior(new FlyWithWings());
        model.perfomFly();
    }
}
