package ru.blm.patterns.strategy.interfaces;

public interface QuackBehavior {
    public void quack();
}
