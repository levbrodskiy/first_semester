package ru.blm.patterns.strategy.interfaces;

public interface FlyBehavior {
    public void fly();
}
