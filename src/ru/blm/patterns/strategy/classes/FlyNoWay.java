package ru.blm.patterns.strategy.classes;

import ru.blm.patterns.strategy.interfaces.FlyBehavior;

public class FlyNoWay implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("I don't flying!");
    }
}
