package ru.blm.patterns.strategy.classes;

import ru.blm.patterns.strategy.interfaces.QuackBehavior;

public class Quack implements QuackBehavior {

    @Override
    public void quack() {
        System.out.println("Quack!");
    }
}
