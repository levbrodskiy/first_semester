package ru.blm.multiplication;

import org.junit.Test;

import static org.junit.Assert.*;

public class MathematicalOperationTest {

    @Test
    public void multiplyPositive() {
        int a = 100 + (int) (Math.random() * 200);
        int b = 100 + (int) (Math.random() * 100);

        assertEquals(49, MathematicalOperation.multiplication(7, 7));
        assertEquals(a * b, MathematicalOperation.multiplication(a, b));
    }

    @Test
    public void multiplyZeroByZero() {
        int a = 0;
        int b = 0;

        assertEquals(0, MathematicalOperation.multiplication(a, b));
    }

    @Test
    public void multiplyNegative() {
        int a = -100 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 100);

        assertEquals(49, MathematicalOperation.multiplication(-7, -7));
        assertEquals(a * b, MathematicalOperation.multiplication(a, b));
    }
    @Test
    public void multiplyNegativeAndPositive() {
        int a = -100 + (int) (Math.random() * 100);
        int b = 100 + (int) (Math.random() * 100);

        assertEquals(-49, MathematicalOperation.multiplication(-7, 7));
        assertEquals(a * b, MathematicalOperation.multiplication(a, b));
    }
    @Test
    public void multiplyZeroByNum() {
        int a = -100 + (int) (Math.random() * 200);
        int b = 0;
        assertEquals(0, MathematicalOperation.multiplication(7, 0));
        assertEquals(a * b, MathematicalOperation.multiplication(a, b));
    }
}